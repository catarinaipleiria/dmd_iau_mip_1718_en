//
//  FirstViewController.swift
//  DMD_IAU_MPI_1718_EN
//
//  Created by Catarina Silva on 21/09/2017.
//  Copyright © 2017 ipleiria. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var firstViewLabelText: UILabel!
    
    @IBAction func firstViewButtonPressed(_ sender: UIButton) {
        firstViewLabelText.text = "Ok, button pressed!"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

